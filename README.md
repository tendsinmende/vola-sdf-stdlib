<div align="center">

# Vola SDF stdlib

**VO**lume **LA**nguage: A research [DSL](https://en.wikipedia.org/wiki/Domain-specific_language) that evaluates volume functions.

The [SDF](https://en.wikipedia.org/wiki/Signed_distance_function) standard library for [Vola](https://gitlab.com/tendsinmende/vola).

[![dependency status](https://deps.rs/repo/gitlab/tendsinmende/vola/status.svg)](https://deps.rs/repo/gitlab/tendsinmende/vola)
[![MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-blue)](LICENSE)

</div>

## Usage

`std.vola` defines all concepts used in the standard library. Those are:

- `Sdf3d`
- `Sdf2d`
- `Color`

All other files implement one entity or operation, that implements all concepts that apply. So for instance 
`Box` implements `Sdf3d` and `Color`, but not `Sdf2d` since that is implemented by `Rectangle` instead.

If you want to use one of the entities or operations, just use the `module xy;` syntax to include them in your file. Optionally include `std` to 
implement the standard concepts for custom entities or operations.

There are also some utility modules like `noise`, that implement common reusable functions.

Finally there is `prelude.vola`, that just includes _everything_, if you don't care about compile times 😺.

## License

Licensed under

Mozilla Public License Version 2.0 ([LICENSE](LICENSE) or <https://www.mozilla.org/en-US/MPL/2.0/>)


### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the MPL-2.0 license, without any additional terms or conditions.

